package com.example.elastic.springelasticpractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringElasticPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringElasticPracticeApplication.class, args);
	}

}
